﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer.Test.Dummies
{
    public class Person
    {
        [CompareableField("Imię")]
        public string Name { get; set; }

        [CompareableField("Data urodzenia")]
        public DateTime BirthDate { get; set; }

        [CompareableField("ID osoby")]
        public int Id { get; set; }

        [ComplexCompareableField]
        public AddressViewModel Address { get; set; }

        [CompareableField]
        public IEnumerable<int> Parents { get; set; }

        [CompareableField]
        public IEnumerable<int> Children { get; set; }

        [ComplexCompareableField]
        public IAnimal Pet { get; set; }
    }

    public class AddressViewModel
    {
        [CompareableField("Ulica")]
        public string Street { get; set; }

        [CompareableField("Numer ulicy")]
        public int Streetnumber { get; set; }
    }


    public interface IAnimal
    {
        [CompareableField]
        string Name { get; set; }
    }

    public class HomeAnimal : IAnimal
    {
        public string Name { get; set; }
    }
}
