﻿using Ninject;
using ObjectComparer.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer.Tests
{
    public class Utils
    {
        public static IKernel Container { get { return container; } }

        static IKernel container;

        static Utils()
        {
            PrepareContainer();
        }

        static void PrepareContainer()
        {
            container = new StandardKernel();
            container.Bind<IComparing>().To<ObjectComparer>();
        }

    }
}
