﻿using System;
using NUnit;
using NUnit.Framework;
using ObjectComparer.Contracts;
using Ninject;
using ObjectComparer.Test.Dummies;
using System.Collections.Generic;
using System.Linq;

namespace ObjectComparer.Tests
{
    [TestFixture]
    public class ObjectComparerTests
    {
        IComparing _objectComparer;

        [SetUp]
        public void Setup()
        {
            _objectComparer = Utils.Container.Get<IComparing>();
        }

        [TearDown]
        public void Clear()
        {
            _objectComparer = null;
        }

        [Test]
        public void Compare_two_completly_diffrent_objects_but_the_same_type()
        {
            var janek = DummiesFactory.Janek;
            var marta = DummiesFactory.Marta;

            var diffs = _objectComparer.Compare(marta, janek);

            Assert.AreEqual(6, diffs.Count());
        }

        [Test]
        public void Compare_two_the_same_objects()
        {
            var janek1 = DummiesFactory.Janek;
            var marta2 = DummiesFactory.Janek;

            var diffs = _objectComparer.Compare(marta2, janek1);

            Assert.AreEqual(0, diffs.Count());
        }
    }

    public static class DummiesFactory
    {
        public static Person Janek
        {
            get
            {
                return new Person
                {
                    Id = 1,
                    BirthDate = new DateTime(1960, 1, 1),
                    Name = "Janek",
                    Address = new AddressViewModel
                    {
                        Street = "Marszałkowska",
                        Streetnumber = 123
                    },
                    Parents = new int[] { 1, 2 },
                    Children = new int[] { 9 },
                    Pet = new HomeAnimal { Name = "Azor" },

                };
            }
        }

        public static Person Marta
        {
            get
            {
                return new Person
                {
                    Id = 8123,
                    BirthDate = new DateTime(2000, 1, 1),
                    Name = "Marta",
                    Address = new AddressViewModel
                    {
                        Street = "Długa",
                        Streetnumber = 897
                    },
                    Parents = new int[] { 2, 3 },
                    Children = new int[0],
                    Pet = null,
                };
            }
        }

    }
}
