﻿using ObjectComparer.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer
{
    public class ObjectComparerWrap
    {
        public static IEnumerable<IDiff> CompareObjects(object obj1, object obj2)
        {
            IComparing comparer = new ObjectComparer();
            return comparer.Compare(obj1, obj2);
        }
    }
}
