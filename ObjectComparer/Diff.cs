﻿using ObjectComparer.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer
{
    public class Diff : IDiff
    {
        public string FieldName { get; set; }
        public object InitVal { get; set; }
        public object FinalVal { get; set; }
    }

    public static class DiffExt
    {
        public static T GetAs<T>(this object obj)
        {
            return (T)obj;
        }

        public static bool TryGetAs<T>(this object obj, out T value)
        {
            try
            {
                value = (T)obj;
                return true;
            }
            catch (InvalidCastException)
            {
                value = default(T);
                return false;
            }
        }

        public static string GetAsString(this object obj)
        {
            if (obj is string)
            {
                return (string)obj;
            }
            if (obj is int || obj is int?)
            {
                return Convert.ToString(obj);
            }
            if (obj is DateTime || obj is DateTime?)
            {
                return ((DateTime)obj).ToString();
            }

            return null;
        }
    }
}
