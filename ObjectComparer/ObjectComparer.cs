﻿using KellermanSoftware.CompareNetObjects;
using ObjectComparer.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer
{
    #region [ Attributes ]
    [AttributeUsage(AttributeTargets.Property)]
    public class CompareableField : Attribute
    {
        public CompareableField(string name = null)
        {
            FieldName = name;
        }

        public string FieldName { get; private set; }
    }

    public class ComplexCompareableField : CompareableField { }

    public class CompareableCollectionField : CompareableField { }

    #endregion

    public class ObjectComparer : IComparing
    {
        protected IDictionary<string, string> ValidNames = new Dictionary<string, string>();
        protected ISet<PropertyInfo> TrackedProperties = new HashSet<PropertyInfo>();

        private readonly int _maxDifferences;
        public const int DEFAULT_MAX_DIFFERENCES = 100;

        #region [ Constructors ]
        public ObjectComparer()
            : this(DEFAULT_MAX_DIFFERENCES)
        {
        }

        public ObjectComparer(int maxDiffs)
        {
            if (_maxDifferences < 0)
                throw new ArgumentException("Max differencies number must be a positive value.");

            _maxDifferences = maxDiffs;
        }

        #endregion

        public IEnumerable<IDiff> Compare(object obj1, object obj2)
        {
            PreparePropertiesList(obj1);

            var comparer = new CompareLogic(GetConfig());
            var result = comparer.Compare(obj1, obj2);

            var diffs = new List<Diff>();
            foreach (var d in result.Differences)
            {
                var diff = new Diff
                {
                    FieldName = ValidNames[d.PropertyName],
                    InitVal = d.Object1.Target,
                    FinalVal = d.Object2.Target
                };

                diffs.Add(diff);
            }

            return diffs;
        }

        protected virtual void PreparePropertiesList(object objToCompare)
        {
            PreparePropertiesList(objToCompare.GetType(), string.Empty); // First invoice of recurrence
        }

        protected virtual void PreparePropertiesList(Type type, string prefix)
        {
            prefix = "." + prefix;

            PrepareComplexProperties(type, prefix);

            PrepareSimpleProperties(type, prefix);
        }

        protected virtual void PrepareComplexProperties(Type type, string prefix)
        {
            var complexProps = GetPropertiesWithAttribute(type, typeof(ComplexCompareableField));
            foreach (var property in complexProps)
                PreparePropertiesList(property.PropertyType, property.Name + prefix);
        }

        protected virtual void PrepareSimpleProperties(Type type, string prefix)
        {
            var simpleProps = GetPropertiesWithAttribute(type, typeof(CompareableField));
            SetValidNames(simpleProps, prefix);
        }

        // TODO involve
        protected virtual void PrepareCollectionProperties(Type type, string prefix)
        {
            var collectionProps = GetPropertiesWithAttribute(type, typeof(CompareableCollectionField));
            SetValidNames(collectionProps, prefix);
        }

        protected IEnumerable<PropertyInfo> GetPropertiesWithAttribute(Type objectType, Type attribute)
        {
            return objectType.GetProperties().Where(p => Attribute.IsDefined(p, attribute));
        }

        protected virtual void SetValidNames(IEnumerable<PropertyInfo> properties, string prefix)
        {
            foreach (var p in properties)
            {
                if (!TrackedProperties.Contains(p))
                {
                    if (!ValidNames.ContainsKey(p.Name))
                    {
                        // If custom property name has been not assigned, default property name will be taken.
                        var validName = (p.GetCustomAttribute(typeof(CompareableField), true) as CompareableField).FieldName ?? p.Name;
                        ValidNames.Add(prefix + p.Name, validName);
                        TrackedProperties.Add(p);
                    }
                }
            }
        }

        protected virtual ComparisonConfig GetConfig()
        {
            ComparisonConfig config = new ComparisonConfig
            {
                MembersToInclude = TrackedProperties.Select(p => p.Name).ToList(),
                MaxDifferences = _maxDifferences,
                CompareChildren = true,
            };

            return config;
        }

    }
}
