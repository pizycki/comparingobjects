﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer.Contracts
{
    public interface IDiff
    {
        string FieldName { get; set; }
        object InitVal { get; set; }
        object FinalVal { get; set; }
    }
}
