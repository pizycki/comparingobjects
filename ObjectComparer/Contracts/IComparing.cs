﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer.Contracts
{
    public interface IComparing
    {
        IEnumerable<IDiff> Compare(object obj1, object obj2);
    }
}
